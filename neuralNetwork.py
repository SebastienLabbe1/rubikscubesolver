import torch
import torch.nn as nn
import torch.nn.functional as F

#https://pytorch.org/tutorials/beginner/blitz/neural_networks_tutorial.html

class NN(nn.Module):
    def __init__(self, cubeLen, k, inter, outputLen):
        super(NN, self).__init__()
        n2 = cubeLen * cubeLen
        self.k = k
        self.c = torch.nn.Conv1d(1, self.k, n2 * 6, 1, dilation=6)
        if len(inter) == 0:
            self.fc = [nn.Linear(6 * self.k, outputLen)]
            return
        self.fc = []
        self.fc.append(nn.Linear(6 * self.k, inter[0]))
        for i in range(len(inter[:-1])):
            self.fc.append(nn.Linear(inter[i], inter[i+1]))
        self.fc.append(nn.Linear(inter[-1], outputLen))

    def forward(self, x):
        x = x.unsqueeze(0).unsqueeze(0)
        x = F.relu(self.c(x))
        x = x.view(-1, self.num_flat_features(x))
        for fc in self.fc[:-1]:
            x = F.relu(fc(x))
        x = self.fc[-1](x)
        return x
    def num_flat_features(self, x):
        size = x.size()[1:]
        num_features = 1
        for s in size:
            num_features *= s
        return num_features

class NN2(nn.Module):
    def __init__(self, cubeLen, inter, outputLen):
        nn.Module.__init__(self)
        n2 = cubeLen * cubeLen
        self.forwards = 0

        if len(inter) == 0:
            self.fc = nn.Linear(n2 * 6 * 6, outputLen)
            self.c = []
            return

        self.c = [torch.nn.Conv1d(1, inter[0], n2 * 6, 1, dilation=6)]
        for i,l in enumerate(inter[:-1]):
            self.c.append(torch.nn.Conv1d(l, inter[i+1], 1))
        self.fc = nn.Linear(inter[-1] * 6, outputLen);
        return


    def forward(self, x):
        self.forwards += 1
        x = x.unsqueeze(0).unsqueeze(0)
        for c in self.c:
            x = F.relu(c(x))
        x = x.view(-1, self.num_flat_features(x))
        x = self.fc(x)
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]
        num_features = 1
        for s in size:
            num_features *= s
        return num_features
