#!/usr/bin/env python3

import visualize
import numpy
import torch
import random
from itertools import product
from neuralNetwork import NN, NN2
import torch.nn as nn

class Cube:
    """
    This is the best rubik's cube class youll ever see
    It has it all, its computationaly expensive, its confusing, its just perfect

    Face orders F U R D L B
    Face orders Front Up Rright Down Left Behind

    Moves:
    F+ +1
    F- -1
    U+ +2
    U- -2
    R+ +3
    R- -3
    D+ +4
    D- -4
    L+ +5
    L- -5
    B+ +6
    B- -6
    """

    centerVector = [numpy.array(x) for x in [
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1],
        [0, -1, 0],
        [0, 0, -1],
        [-1, 0, 0],
        ]]

    diagXY = [[numpy.array(x[0]), numpy.array(x[1])] for x in [
        [[0, 0, 1], [0, 1, 0]],
        [[0, 0, 1], [-1, 0, 0]],
        [[0, -1, 0], [-1, 0, 0]],
        [[0, 0, -1], [-1, 0, 0]],
        [[0, 1, 0], [-1, 0, 0]],
        [[0, 0, 1], [0, -1, 0]],
        ]]


    neighs = [[x, x[::-1]] for x in [
        [1, 2, 3, 4],
        [5, 2, 0, 4],
        [5, 3, 0, 1],
        [5, 4, 0, 2],
        [5, 1, 0, 3],
        [3, 2, 1, 4]
        ]]

    neighRots = [[x, [x[0][::-1], x[1][::-1]]] for x in [
        [[0, 0, 0, 0], [1, 1, 1, 1]],
        [[0, 6, 8, 2], [1, -3, -1, 3]],
        [[2, 6, 2, 2], [3, -3, 3, 3]],
        [[8, 6, 0, 2], [-1, -3, 1, 3]],
        [[6, 6, 6, 2], [-3, -3, -3, 3]],
        [[8, 8, 8, 8], [-1, -1, -1, -1]],
        ]]

    faceRot = [[x, x[::-1]] for x in [
        [1, 3, 7, 5],
        [0, 6, 8, 2]
        ]]

    comute = {(1,6), (5,3), (2,4)}


    def __init__(self, n):
        self.n = n
        self.n2 = n * n
        self.flatLen = self.n2 * 6 * 6
        self.c = [j for j in range(6) for i in range(self.n2)]

    def reset(self):
        for j, i in product(range(6), range(self.n2)):
            self.c[j * 9 + i] = j

    def move(self, iMove):
        iF = abs(iMove) - 1
        iFB = iF * self.n2
        iD = 0 if iMove > 0 else 1
        for trot in Cube.faceRot:
            rot = trot[iD]
            cache = self.c[iFB + rot[0]]
            for i in range(3):
                self.c[iFB + rot[i]] = self.c[iFB + rot[i+1]]
            self.c[iFB + rot[3]] = cache
        rotFace = Cube.neighs[iF][iD]
        rotInit, rotStep = Cube.neighRots[iF][iD]
        rot = rotInit.copy()
        for j in range(self.n):
            cache = self.c[rotFace[0] * self.n2 + rot[0]]
            for i in range(3):
                self.c[rotFace[i] * self.n2 + rot[i]] = self.c[rotFace[i+1] * self.n2 + rot[i+1]]
            self.c[rotFace[3] * self.n2 + rot[3]] = cache
            if j < 2:
                for i in range(4):
                    rot[i] += rotStep[i]
    
    def visualize(self):
        vis = visualize.FasterVisualizer()
        z = (self.n - 1) / 2
        for i in range(6):
            centerPos = Cube.centerVector[i]
            X, Y = Cube.diagXY[i]
            for j, k in product(range(self.n), range(self.n)):
                iC = i * self.n2 + j * self.n + k
                c = self.c[iC]
                pos = centerPos + ((k - z) * X + (j - z) * Y) * 2/self.n
                diag = (X + Y) * 0.9/self.n
                color = (visualize.tupcolors[c][0], visualize.tupcolors[c][1], visualize.tupcolors[c][2], 1)
                vs, fs = visualize.gen_mesh_cube(pos, diag)
                vis.add_mesh(vs, fs, color)
        vis.show_plot()

    def randomMove():
        return random.randint(1,6) * (random.randint(0,1) * 2 - 1)

    def cleanPath(path):
        done = False
        while not done:
            change = False
            for i in range(len(path) - 1):
                if (abs(path[i]), abs(path[i+1])) in Cube.comute and abs(path[i]) > abs(path[i+1]):
                    path[i],path[i+1] = path[i+1],path[i]
                    change = True
            if change:
                continue
            for i in range(len(path) - 1):
                if path[i] + path[i+1] == 0:
                    path.pop(i)
                    path.pop(i)
                    change = True
                    break
            done = not change
        return path

    def randomPath(length):
        path = []
        while len(path) == 0:
            path = [Cube.randomMove() for i in range(3*length)]
            #print("creating path of length {}".format(length))
            #print(path)
            path = Cube.cleanPath(path)
            path = path[:min(len(path),length)]
            #print(path)
        return path

    def shuffle(self, moves = 10):
        for move in Cube.randomPath(moves):
            self.move(move)

    def toList(self):
        x = [0] * (self.flatLen)
        for i in range(self.n2 * 6):
            x[i * 6 + self.c[i]] = 1
        return x
    
    def __hash__(self):
        return hash(tuple(self.c))
    
    def isFinished(self):
        for i in range(6):
            for j in range(1, self.n2):
                if self.c[i * 9] != self.c[i * 9 + j]:
                    return False
        return True
                
    
def label_to_move(i):
    return i//2 + 1 if not i % 2 else -(i//2 + 1)

def move_to_label(move):
    return (abs(move) - 1) * 2 + (1 if move < 0 else 0)

def output_to_label(x):
    return x.squeeze(0).max(0)[1]

def onehot(i):
    return torch.nn.functional.one_hot(torch.as_tensor([i]), 12)

def output_to_move(x):
    return label_to_move(output_to_label(x))

def move_to_target(move):
    return onehot(move_to_label(move))

def learnEpisode(nnet, criterion, suffles = 10):
    #not sure this still works
    cube = Cube(3)
    cube.shuffle(suffles)
    optimizer = torch.optim.SGD(nnet.parameters(), lr=0.01)
    seen = set()
    pairs = []
    i = 0
    while not cube.isFinished() and cube not in seen and i < 30:
        i += 1
        seen.add(cube)
        flatCube = torch.as_tensor(cube.toList())
        output = nnet.forward(flatCube.float())
        label = output_to_label(output)
        move = label_to_move(label.item())
        cube.move(move)
        pairs.append((output, label))
    optimizer.zero_grad()
    if cube.isFinished():
        for o, l in pairs:
            loss = criterion(o.unsqueeze(0), l.unsqueeze(0))
            loss.backward()
    else:
        for o, l in pairs:
            loss = criterion(o.unsqueeze(0), torch.as_tensor([random.randint(0, 11)]))
            loss.backward()
    optimizer.step()

def learn(nnet, criterion, cube, explorationRate = 0.1):
    #not sure this still works
    seen = set()
    pairs = []
    maxMoves = 30
    i = 0
    while not cube.isFinished() and cube not in seen and i < maxMoves:
        i += 1
        seen.add(cube)
        flatCube = torch.as_tensor(cube.toList())
        output = nnet.forward(flatCube.float())
        if random.random() < explorationRate:
            label = torch.as_tensor([random.randint(0, 11)])
        else:
            label = output_to_label(output).unsqueeze(0)
        move = label_to_move(label.item())
        cube.move(move)
        pairs.append((output.unsqueeze(0), label))
    finished = cube.isFinished()
    for o, l in pairs:
        target = onehot(l.item())
        if not finished:
            target = 1 - target
        target = target.float()
        loss = criterion(o, target)
        loss.backward()
    return i if finished else maxMoves

def explorationLearning(nnet, criterion, optimizer, maxSteps=1, iterations=1000, batchSize=100, explorationRate=0.1):
    #not sure this still works
    for i in range(iterations):
        optimizer.zero_grad()
        totalMoves = 0
        for j in range(batchSize):
            cube = Cube(3)
            cube.shuffle(random.randint(1,maxSteps))
            totalMoves += learn(nnet, criterion, cube, explorationRate)
        avgMoves = totalMoves/batchSize
        progress(i, iterations, avgMoves)
        optimizer.step()

def testPath(nnet, path):
    CUBE.reset()
    for move in path[::-1]:
        CUBE.move(-move)
    seen = set()
    i = 0
    moves = []
    while not CUBE.isFinished() and CUBE not in seen and i < 30:
        i += 1
        seen.add(CUBE)
        flatCube = torch.as_tensor(CUBE.toList())
        output = nnet.forward(flatCube.float())
        move = output_to_move(output)
        CUBE.move(move)
        moves.append(move.item())
    return CUBE.isFinished(), moves

def learnFromPath(nnet, criterion, path, optimizer):
    CUBE.reset()
    for move in path[::-1]:
        CUBE.move(-move)
    acc = 0
    inv = 1/2
    for i,move in enumerate(path):
        #prediction
        #print(CUBE.toList())
        flatCube = torch.as_tensor(CUBE.toList())
        output = nnet.forward(flatCube.float())

        #Accurary
        if output_to_label(output) == move_to_label(move):
            acc += 1

        #target
        #target = move_to_target(move).squeeze(0).float()
        label = torch.as_tensor([move_to_label(move)])

        #learning
        loss = criterion(output, label)
        loss.backward()
        #loss.backward(torch.Tensor([1/(i+1)]))
        
        #move CUBE
        CUBE.move(move)
    return acc/len(path)

def solve(nnet, cube, vis=False):
    seen = set()
    i = 0
    if vis: cube.visualize()
    while not cube.isFinished() and cube not in seen and i < 30:
        i += 1
        seen.add(cube)
        flatCube = torch.as_tensor(cube.toList())
        output = nnet.forward(flatCube.float())
        label = output_to_label(output)
        move = label_to_move(label.item())
        cube.move(move)
        if vis: cube.visualize()
    return (cube.isFinished(), i)

def progress(i, iterations, progressLen = 20, extra = ""):
    p = (i * progressLen) //iterations
    bar = ("X" * p) + (" " * (progressLen - p))
    print(" "*125, end="\r")
    print("Learning iter {} : ||{}|| {}".format(i, bar, extra), end="\r")


def testNet(nnet, tests, maxSteps=1):
    solves = 0
    totalMoves = 0
    for i in range(tests):
        CUBE.reset()
        CUBE.shuffle(maxSteps)
        solved, moves = solve(nnet, CUBE)
        if solved:
            solves += 1
            totalMoves += moves
    solveProb = solves / tests
    avgMoves = totalMoves / solves if solves > 0 else 1000000000
    print("Solve probability : {} Average Moves : {}".format(solveProb, avgMoves))
    return solveProb, avgMoves

def pathLearning(nnet, criterion, optimizer, maxSteps, iterations=100, batchSize=1000):
    for i in range(iterations):
        totalAccuracy = 0
        for j in range(batchSize):
            optimizer.zero_grad()
            path = Cube.randomPath(random.randint(1, maxSteps))
            #print(path)
            totalAccuracy += learnFromPath(nnet, criterion, path, optimizer)
            optimizer.step()
        accuracy = totalAccuracy / batchSize
        solveR = accuracy ** maxSteps
        extra = "Accuracy : {} Solve Prob : {}  FROWARDS : {}".format(accuracy, solveR, nnet.forwards)
        progress(i, iterations, extra = extra)

#VARIABLES
CUBE = Cube(3)
MAX_STEPS = 5
#nnet = NN(3, 1000, [], 12)
nnet = NN2(3, [], 12)
criterions = [
    nn.MSELoss(),
    nn.CrossEntropyLoss()
]
iC = 1

optimizers = [
    torch.optim.SGD(nnet.parameters(), lr=0.05),
    torch.optim.Adadelta(nnet.parameters()),
    torch.optim.Adagrad(nnet.parameters())
]
iP = 0

print("Testing solver with a maximum of {} moves".format(MAX_STEPS))
optimizer = optimizers[iP]
print("Using optimizer number {}: ".format(iP), optimizer.state_dict()['param_groups'])
criterion = criterions[iC]

pathLearning(nnet, criterion, optimizer, MAX_STEPS)
print()

testNet(nnet, 100, MAX_STEPS)

for i in range(10):
    path = Cube.randomPath(MAX_STEPS)
    solved, cPath = testPath(nnet, path)
    print("test", i)
    print("results",solved)
    print("path", path)
    print("nnet path", cPath)


for i in range(0):
    CUBE.reset()
    CUBE.shuffle(MAX_STEPS)
    solve(nnet, CUBE, vis=True)

